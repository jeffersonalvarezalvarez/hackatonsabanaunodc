class DropPostsTable < ActiveRecord::Migration[5.1]
  def up
  	drop_table :posts
  	drop_table :recognitions
  	drop_table :recordings
  	drop_table :texts
  end

  def down
  	raise ActiveRecord::IrreversibleMigration
  end

end
